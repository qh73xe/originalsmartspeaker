# -*- coding: utf-8 -*
"""形態素解析APIサーバー

このファイルでは基本的に各種 API 自身の作成のみを行います.
種々の解析自体はそれぞれ別途モジュールを呼び出します.
"""
import json
from tornado.web import RequestHandler, Application
from tornado.escape import json_decode, url_unescape
from tornado.options import define, options

define("port", default=8888, help="run on the given port", type=int)


class MainHandler(RequestHandler):
    def get(self):
        self.write("Hello, world")


class MecabHandler(RequestHandler):

    def initialize(self):
        """Mecab モジュールのインスタンス化"""
        from mecab import Mecab
        self.mecab = Mecab()

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def get(self, *args, **kwargs):
        text = self.request.arguments.get('text', [])
        if text:
            text = url_unescape(text[0])
        else:
            text = None
        self.write(json.dumps(
            self.morphologicalAnalysis(text), ensure_ascii=False)
        )

    def post(self):
        request_json = json_decode(self.request.body)
        text = request_json.get('text', None)
        self.write(json.dumps(
            self.morphologicalAnalysis(text), ensure_ascii=False)
        )

    def morphologicalAnalysis(self, text):
        if text:
            result = self.mecab.parse(text)
            return {
                'length': len(result),
                'contents': result
            }
        else:
            return {
                'length': 0,
                'contents': []
            }


class ComainuHandler(RequestHandler):

    def initialize(self):
        self._url = "http://comainu.org/try_comainu/"

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def get(self, *args, **kwargs):
        text = self.request.arguments.get('text', [])
        if text:
            text = url_unescape(text[0])
        else:
            text = None
        result = self.morphologicalAnalysis(text)
        self.write(json.dumps(
            {
                'length': len(result),
                'contents': result
            }, ensure_ascii=False
        ))

    def post(self):
        request_json = json_decode(self.request.body)
        text = request_json.get('text', None)
        if text:
            result = self.morphologicalAnalysis(text)
            self.write(json.dumps(
                {
                    'length': len(result),
                    'contents': result
                }, ensure_ascii=False
            ))
        else:
            self.write(json.dumps(
                {
                    'length': 0,
                    'contents': []
                }, ensure_ascii=False
            ))

    def morphologicalAnalysis(self, text):
        from requests import post
        from bs4 import BeautifulSoup
        res = post(self._url, {'sentence': text})
        soup = BeautifulSoup(res.text, 'lxml')
        table = soup.find('table')
        heads = [h.text for h in table.findAll('th')[-6:]]
        return [
            dict(zip(heads, v.split('\t')[-6:]))
            for v in '\t'.join(
                [v.text for v in table.findAll('td')]
            ).split('\n')[:-2]
        ]


def make_app():
    """URL 設計関数."""
    return Application([
        (r"/", MainHandler),
        (r"/mecab", MecabHandler),
        (r"/comainu", ComainuHandler),
    ])


if __name__ == "__main__":
    from tornado.ioloop import IOLoop
    options.parse_command_line()
    app = make_app()
    app.listen(options.port)
    ioloop = IOLoop.current()
    try:
        print(
            'Morphological-API: START WITH http://localhost:{}'.format(
              str(options.port)
            )
        )
        ioloop.start()
    except KeyboardInterrupt:
        print('Morphological-API: END')
        ioloop.stop()
