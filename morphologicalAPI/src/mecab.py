# -*- coding: utf-8 -*
"""Mecab モジュール

Mecab を使用し形態素解析を行います.
なお，本アプリケーションでは unidic を辞書として使用することを前提としています.

出力フォーマットは Docker 上では /var/lib/mecab/dic/debian/dicrc で定義されています.
これを変更する場合は Docker の mount 機能を有効活用するべきでしょう.
ここの詳細に関しては以下のページを確認してください.

- http://unidic.ninjal.ac.jp/faq

"""

HEADERS = [
    'pos',  # 品詞大分類
    'pos2',  # 品詞中分類
    'pos3',  # 品詞小分類
    'pos4',  # 品詞細分類
    'cType',  # 活用型
    'cForm',  # 活用形
    'lForm',  # 語彙素読み
    'lemma',  # 語彙素（＋語彙素細分類）
    'orth',   # 書字形出現形
    'pron',   # 発音形出現形
    'orthBase',  # 書字形基本形
    'pronBase',  # 発音形基本形
    'goshu',  # 語種
    'iType',  # 語彙素類
    'iForm',  # 語頭変化形
    'fType',  # 語末変化型
    'fForm'  # 語末変化形
]


class Mecab(object):

    _unk_char = '*'
    _headers = [
        'word',
        HEADERS[9],
        HEADERS[6],
        HEADERS[7],
        HEADERS[0],
        HEADERS[4],
        HEADERS[5],
    ]

    def __init__(self):
        from MeCab import Tagger
        self._tagger = Tagger()

    def _as_dict(self, lines):
        """Mecab の出力を辞書形式に変換します"""
        if len(self._headers) == len(lines):
            return {
                self._headers[i]: lines[i]
                for i in range(len(self._headers))
            }
        else:
            values = [lines[0]] + ['*' for i in range(1, len(self._headers))]
            return {
                self._headers[i]: values[i]
                for i in range(len(self._headers))
            }

    def parse(self, text):
        return [
            self._as_dict(line.split('\t'))
            for line in self._tagger.parse(text).split('\n')[:-2]
        ]
