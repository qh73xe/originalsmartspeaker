K-lab: 形態素解析 API
==================================================

この Docker ファイルは日本語形態素解析サーバーを提供します.
バックエンドの技術としては, mecab, comainu, CaboCha を使用し,
サーバーには python/tornado を利用しています.

Useage
-------------------------------------------------------------

以下のコマンドを実行すると, サーバープロセスが立ち上がります.

.. code-block:: bash

   $ docker run -itd -p 8888:8888 qh73xe/morphologicalapi

後はブラウザを開き以下の様にクエリを投げます.

短単位解析:
   http://localhost:8888/mecab?text=今日はいい天気だ

長単位解析:
   http://localhost:8888/comainu?text=今日はいい天気だ

http method
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

上記の例は get method ですが post method を使用することも可能です.

URL::

   http://localhost:8888/mecab

BODY::

   {
      "text": "今日はいい天気です"
   }

参考
-------------------------------------------------------------

- mecab: http://taku910.github.io/mecab/
- cabocha: http://taku910.github.io/cabocha/
- comainu: http://comainu.org/
