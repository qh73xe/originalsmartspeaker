# -*- coding: utf-8 -*
"""指定された ファイル を base64 形式に変換します."""


def to_base64(fpath):
    from base64 import b64encode
    with open(fpath, "rb") as f:
        return b64encode(f.read())


def to_urlsafe_base64(fpath):
    from base64 import urlsafe_b64encode
    with open(fpath, "rb") as f:
        return urlsafe_b64encode(f.read())


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(
        prog='show_base64.py',
        usage='python show_base64.py -f ../misc/sample.wav',
        description='show base64',
        add_help=True,
    )
    parser.add_argument(
      '-f', '--file',
      help='file path',
      required=True
    )
    parser.add_argument(
        '-c', '--clip',
        action='store_true',
        help='copy base64 (require python/pyperclip)',
    )
    parser.add_argument(
        '--urlsafe',
        action='store_true',
        help='enc url safe base64 (require python/pyperclip)',
    )

    args = parser.parse_args()
    if args.urlsafe:
        result = to_urlsafe_base64(args.file)
    else:
        result = to_base64(args.file)
    if args.clip:
        from pyperclip import copy
        copy(result.decode())
    print(result)
