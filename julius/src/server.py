# -*- coding: utf-8 -*
"""形態素解析APIサーバー

このファイルでは基本的に各種 API 自身の作成のみを行います.
種々の解析自体はそれぞれ別途モジュールを呼び出します.
"""
import json
from os import path
from uuid import uuid4

from tornado.web import RequestHandler, Application
from tornado.escape import json_decode
from tornado.options import define, options

from julius import segment,  Dictation

define("port", default=8888, help="run on the given port", type=int)
DICTATION = Dictation()

ROOT = path.dirname(path.abspath(__file__))
TEMPDIR = path.join(ROOT, 'tmp')


class MainHandler(RequestHandler):

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def get(self):
        result = {
            'title': 'Wellcom to Speech-Recognition-API',
            'description': 'you can get the text of wav.',
            'details': [
                {
                    'url': 'localhost/:<port>',
                    'description': 'the wellcom page'
                },
                {
                    'url': 'localhost:<port>/segmentation',
                    'description': 'run julius segmentation-tool-kit'
                },
                {
                    'url': 'localhost:<port>/dictation',
                    'description': 'run julius dictation-tool-kit'
                }
            ]
        }
        self.write(json.dumps(
            result, ensure_ascii=False
        ))


class BaseHandler(RequestHandler):
    """julius 処理系の基底ハンドラ"""

    def initialize(self):
        self.wfile = None

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def get(self):
        result = {
            'status': 'error',
            'reason': 'get method is not supported.'
        }
        self.write(json.dumps(
            result, ensure_ascii=False
        ))

    def upload(self, enced_base64):
        from base64 import b64decode
        self.wfile = path.join(TEMPDIR, str(uuid4()) + '.wav')
        with open(self.wfile, "wb") as wf:
            wf.write(b64decode(enced_base64))

    def on_finish(self):
        from os import remove
        if self.wfile:
            if path.exists(self.wfile):
                remove(self.wfile)


class DictationHandler(BaseHandler):
    """julius を使い音声認識を行います."""

    def post(self):
        request_json = json_decode(self.request.body)
        wav = request_json.get('wav', None)
        if wav:
            self.upload(wav)
            sentences = DICTATION.dictation(self.wfile)
            result = {
                'status': 'success',
                'body': sentences,
                'length': len(sentences)
            }
        else:
            result = {
                'status': 'error',
                'reason': 'text and wav are required'
            }
        self.write(json.dumps(
            result, ensure_ascii=False
        ))
        self.finish()


class SegmentationHandler(BaseHandler):
    """julius を使い Segmentation を行います."""

    def post(self):
        request_json = json_decode(self.request.body)
        text = request_json.get('text', None)
        wav = request_json.get('wav', None)
        if text is not None and wav is not None:
            self.upload(wav)
            segments = segment(self.wfile, text)
            result = {
                'status': 'success',
                'body': segments,
                'length': len(segments)
            }
        else:
            result = {
                'status': 'error',
                'reason': 'text and wav are required'
            }
        self.write(json.dumps(
            result, ensure_ascii=False
        ))
        self.finish()

    def on_finish(self):
        from os import remove
        remove(self.wfile)


def make_app():
    """URL 設計関数."""
    return Application([
        (r"/", MainHandler),
        (r"/segmentation", SegmentationHandler),
        (r"/dictation", DictationHandler),
    ])


if __name__ == "__main__":
    from tornado.ioloop import IOLoop
    options.parse_command_line()
    app = make_app()
    app.listen(options.port)
    ioloop = IOLoop.current()
    try:
        print(
            'Speech-Recognition-API: START WITH http://localhost:{}'.format(
              str(options.port)
            )
        )
        ioloop.start()
    except KeyboardInterrupt:
        print('Speech-Recognition-API: END')
        DICTATION.kill()
        ioloop.stop()
