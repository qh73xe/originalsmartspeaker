# -*- coding: utf-8 -*
"""このスクリプトは JULIUS segmentation-kit の python 版になります.

公式の perl スクリプトはいろんな意味で融通が聞かないので python 版に書き直します.

- 参考:
    https://github.com/julius-speech/segmentation-kit/blob/master/segment_julius.pl

"""
from os import path
from subprocess import Popen, PIPE

ROOT = path.dirname(path.abspath(__file__))
TEMPDIR = path.join(ROOT, 'tmp')
MODELDIR = path.join(ROOT, 'models', 'seg')

JULIUSBIN = '/usr/local/bin/julius'
STRKEY = '=== begin forced alignment ==='
ENDKEY = '=== end forced alignment ==='
DICTATIONKEY = '### read waveform input'


class Segmentation(object):

    def __init__(self, julius=JULIUSBIN, acs_model='monophone', **kwargs):
        """Julius をセグメンテーション用に設定します.

        Args:
            julius (str) : julius 実行ファイルへのパス
            acs_model (str) : 音響モデルの種類. 'monophone' or 'triphone'.
            offset_align (float) : offset for result in ms
            disable_silence_at_ends (bool) :
                inserting silence at begin/end of sentence, or not.
        """
        self.juliusbin = julius  # julius executable
        # acoustic model
        if acs_model == 'monophone':
            self.hmmdefs = path.join(
              MODELDIR, 'hmmdefs_monof_mix16_gid.binhmm'
            )
            self.hlist = None
        elif acs_model == 'triphone':
            self.hmmdefs = path.join(MODELDIR, 'hmmdefs_ptm_gid.binhmm')
            self.hmmdefs = path.join(MODELDIR, 'logicalTri')
        self.offset_align = kwargs.get('offset_align', 0.0125)
        self.disable_silence_at_ends = kwargs.get(
            'disable_silence_at_ends', False
        )

    def _to_dfa(self, labels):
        num = len(labels)
        dfalines = [
            '%d %d %d 0 1' % (i, num - 1 - i, i + 1)
            if i == 0 else
            '%d %d %d 0 0' % (i, num - 1 - i, i + 1)
            for i in range(num)
        ]
        dfalines.append('%d -1 -1 1 0' % num)
        return '\n'.join(dfalines)

    def _to_dic(self, labels):
        dic_lines = [
            '{i} [w_{i}] {w}'.format(i=i, w=w)
            for i, w in enumerate(labels)
        ]
        return '\n'.join(dic_lines)

    def save_dfa(self, text):
        from uuid import uuid4
        fileID = str(uuid4())
        dfa = path.join(TEMPDIR, fileID + '.dfa')
        dic = path.join(TEMPDIR, fileID + '.dict')
        label = yomi2voca(text).strip()
        if self.disable_silence_at_ends is not True:
            labels = ['silB', label, 'silE']
        with open(dfa, mode='w') as fh:
            fh.write(self._to_dfa(labels))
        with open(dic, mode='w') as fh:
            fh.write(self._to_dic(labels))
        return dfa, dic

    def clean(self, dfa, dic):
        from os import remove
        remove(dfa)
        remove(dic)

    def init_proc(self, dfa, dic):
        cmds = [
          self.juliusbin,
          '-h', self.hmmdefs,
          '-dfa', dfa,
          '-v', dic,
          '-palign'
        ]
        if self.hlist:
            cmds.extend(['-hlist', self.hlist])
        cmds.extend(['-input', 'file'])
        proc = Popen(
            cmds, encoding='utf8', stdin=PIPE, stdout=PIPE, stderr=PIPE
        )
        return proc

    def as_dict(self, result):
        """segmentation の結果を受取り, dict 型に変換します."""
        from re import sub

        def _as_dict(line):
            lines = line.split()
            return {
              'startTime': round(int(lines[0]) * 0.01, 4),
              'endTime': round(int(lines[1]) * 0.01, 4),
              'seg': lines[-1]
            }

        lines = [
            l.strip()
            for l in result.split('\n')
        ]
        lines = [
            _as_dict(
                sub(r'\s+', " ", l.replace('[', '').replace(']', '')).strip()
            )
            for l in lines[lines.index(STRKEY):lines.index(ENDKEY)]
            if '[' in l
        ]
        return lines

    def segmente(self, wav, text):
        """wav ファイルパスとその内容を受取り時間情報を算出します."""
        dfa, dic = self.save_dfa(text)
        proc = self.init_proc(dfa, dic)
        stdout, stderr = proc.communicate(wav)
        if stdout:
            print(stdout)
            res = self.as_dict(stdout)
        else:
            print(stderr)
            res = []
        self.clean(dfa, dic)
        return res


class Dictation(object):
    """julius Dictation モード実行クラス."""

    _proc = None

    def __init__(self, julius=JULIUSBIN, **kwargs):
        """Julius 実行に必要がコマンドのパスを指定します.

        Args:
            julius (str) : julius 実行ファイルへのパス
            main_conf (str) : main.jconf までのパス
            am_conf (str) : am-dnn.jconf までのパス
            dnn_conf (str) : julius.dnnconf までのパス
        """
        # julius executable
        self.juliusbin = julius
        self.conf = {
            'main': kwargs.get('main_conf', '/dictation-kit/main.jconf'),
            'am': kwargs.get('am_conf', '/dictation-kit/am-dnn.jconf'),
            'dnn': kwargs.get('dnn_conf', '/dictation-kit/julius.dnnconf')
        }
        self._init_dictation_proc()

    def _init_dictation_proc(self):
        """julius の音声認識用プロセスを起動させてこのクラスの管理化に置きます."""
        if self._proc is None:
            cmds = [
              self.juliusbin,
              '-C', self.conf['main'],
              '-C', self.conf['am'],
              '-dnnconf', self.conf['dnn'],
              '-input', 'file'
            ]
            self._proc = Popen(
                cmds, encoding='utf8',
                stdout=PIPE, stderr=PIPE, stdin=PIPE,
            )

    def kill(self):
        if self._proc is not None:
            self._proc.kill()

    def run_dictation(self, wav):
        return self._proc.communicate(wav)

    def as_dict(self, stdout):
        lines = stdout.split('\n')
        lines = lines[lines.index(DICTATIONKEY):]
        sentences = [l for l in lines if 'sentence1' in l]
        wseqs = [l for l in lines if 'wseq1' in l]
        cmscores = [l for l in lines if 'cmscore1' in l]
        res = [
            {
                'sentence': sen.split(':')[-1].strip(),
                'details': [
                    {
                        'word': w.split('+')[0],
                        'pos': w.split('+')[-1],
                        'score': c
                    }
                    for c, w in zip(
                        cms.split(':')[-1].strip().split(),
                        wse.split(':')[-1].strip().split()
                    )
                ]
            }
            for sen, cms, wse in zip(sentences, cmscores, wseqs)
        ]
        return res

    def dictation(self, wav):
        stdout, stderr = self.run_dictation(wav)
        if stdout:
            res = self.as_dict(stdout)
        else:
            res = []
            print(stderr)
        return res


def yomi2voca(text):
    # ゔ行
    text = text.replace('ゔ', ' b u')
    text = text.replace('ゔぁ', ' b a')
    text = text.replace('ゔぃ', ' b i')
    text = text.replace('ゔぇ', ' b e')
    text = text.replace('ゔぉ', ' b o')
    text = text.replace('ゔゅ', ' by u')

    # 2文字からなる変換規則
    text = text.replace('あぁ', ' a a')
    text = text.replace('いぃ', ' i i')
    text = text.replace('いぇ', ' i e')
    text = text.replace('いゃ', ' y a')
    text = text.replace('うぅ', ' u:')
    text = text.replace('えぇ', ' e e')
    text = text.replace('おぉ', ' o:')
    text = text.replace('かぁ', ' k a:')
    text = text.replace('きぃ', ' k i:')
    text = text.replace('くぅ', ' k u:')
    text = text.replace('くゃ', ' ky a')
    text = text.replace('くゅ', ' ky u')
    text = text.replace('くょ', ' ky o')
    text = text.replace('けぇ', ' k e:')
    text = text.replace('こぉ', ' k o:')
    text = text.replace('がぁ', ' g a:')
    text = text.replace('ぎぃ', ' g i:')
    text = text.replace('ぐぅ', ' g u:')
    text = text.replace('ぐゃ', ' gy a')
    text = text.replace('ぐゅ', ' gy u')
    text = text.replace('ぐょ', ' gy o')
    text = text.replace('げぇ', ' g e:')
    text = text.replace('ごぉ', ' g o:')
    text = text.replace('さぁ', ' s a:')
    text = text.replace('しぃ', ' sh i:')
    text = text.replace('すぅ', ' s u:')
    text = text.replace('すゃ', ' sh a')
    text = text.replace('すゅ', ' sh u')
    text = text.replace('すょ', ' sh o')
    text = text.replace('せぇ', ' s e:')
    text = text.replace('そぉ', ' s o:')
    text = text.replace('ざぁ', ' z a:')
    text = text.replace('じぃ', ' j i:')
    text = text.replace('ずぅ', ' z u:')
    text = text.replace('ずゃ', ' zy a')
    text = text.replace('ずゅ', ' zy u')
    text = text.replace('ずょ', ' zy o')
    text = text.replace('ぜぇ', ' z e:')
    text = text.replace('ぞぉ', ' z o:')
    text = text.replace('たぁ', ' t a:')
    text = text.replace('ちぃ', ' ch i:')
    text = text.replace('つぁ', ' ts a')
    text = text.replace('つぃ', ' ts i')
    text = text.replace('つぅ', ' ts u:')
    text = text.replace('つゃ', ' ch a')
    text = text.replace('つゅ', ' ch u')
    text = text.replace('つょ', ' ch o')
    text = text.replace('つぇ', ' ts e')
    text = text.replace('つぉ', ' ts o')
    text = text.replace('てぇ', ' t e:')
    text = text.replace('とぉ', ' t o:')
    text = text.replace('だぁ', ' d a:')
    text = text.replace('ぢぃ', ' j i:')
    text = text.replace('づぅ', ' d u:')
    text = text.replace('づゃ', ' zy a')
    text = text.replace('づゅ', ' zy u')
    text = text.replace('づょ', ' zy o')
    text = text.replace('でぇ', ' d e:')
    text = text.replace('どぉ', ' d o:')
    text = text.replace('なぁ', ' n a:')
    text = text.replace('にぃ', ' n i:')
    text = text.replace('ぬぅ', ' n u:')
    text = text.replace('ぬゃ', ' ny a')
    text = text.replace('ぬゅ', ' ny u')
    text = text.replace('ぬょ', ' ny o')
    text = text.replace('ねぇ', ' n e:')
    text = text.replace('のぉ', ' n o:')
    text = text.replace('はぁ', ' h a:')
    text = text.replace('ひぃ', ' h i:')
    text = text.replace('ふぅ', ' f u:')
    text = text.replace('ふゃ', ' hy a')
    text = text.replace('ふゅ', ' hy u')
    text = text.replace('ふょ', ' hy o')
    text = text.replace('へぇ', ' h e:')
    text = text.replace('ほぉ', ' h o:')
    text = text.replace('ばぁ', ' b a:')
    text = text.replace('びぃ', ' b i:')
    text = text.replace('ぶぅ', ' b u:')
    text = text.replace('ふゃ', ' hy a')
    text = text.replace('ぶゅ', ' by u')
    text = text.replace('ふょ', ' hy o')
    text = text.replace('べぇ', ' b e:')
    text = text.replace('ぼぉ', ' b o:')
    text = text.replace('ぱぁ', ' p a:')
    text = text.replace('ぴぃ', ' p i:')
    text = text.replace('ぷぅ', ' p u:')
    text = text.replace('ぷゃ', ' py a')
    text = text.replace('ぷゅ', ' py u')
    text = text.replace('ぷょ', ' py o')
    text = text.replace('ぺぇ', ' p e:')
    text = text.replace('ぽぉ', ' p o:')
    text = text.replace('まぁ', ' m a:')
    text = text.replace('みぃ', ' m i:')
    text = text.replace('むぅ', ' m u:')
    text = text.replace('むゃ', ' my a')
    text = text.replace('むゅ', ' my u')
    text = text.replace('むょ', ' my o')
    text = text.replace('めぇ', ' m e:')
    text = text.replace('もぉ', ' m o:')
    text = text.replace('やぁ', ' y a:')
    text = text.replace('ゆぅ', ' y u')
    text = text.replace('ゆゃ', ' y a:')
    text = text.replace('ゆゅ', ' y u:')
    text = text.replace('ゆょ', ' y o:')
    text = text.replace('よぉ', ' y o:')
    text = text.replace('らぁ', ' r a:')
    text = text.replace('りぃ', ' r i:')
    text = text.replace('るぅ', ' r u:')
    text = text.replace('るゃ', ' ry a')
    text = text.replace('るゅ', ' ry u')
    text = text.replace('るょ', ' ry o')
    text = text.replace('れぇ', ' r e:')
    text = text.replace('ろぉ', ' r o:')
    text = text.replace('わぁ', ' w a:')
    text = text.replace('をぉ', ' o:')

    text = text.replace('でぃ', ' d i')
    text = text.replace('でぇ', ' d e:')
    text = text.replace('でゃ', ' dy a')
    text = text.replace('でゅ', ' dy u')
    text = text.replace('でょ', ' dy o')
    text = text.replace('てぃ', ' t i')
    text = text.replace('てぇ', ' t e:')
    text = text.replace('てゃ', ' ty a')
    text = text.replace('てゅ', ' ty u')
    text = text.replace('てょ', ' ty o')
    text = text.replace('すぃ', ' s i')
    text = text.replace('ずぁ', ' z u a')
    text = text.replace('ずぃ', ' z i')
    text = text.replace('ずぅ', ' z u')
    text = text.replace('ずゃ', ' zy a')
    text = text.replace('ずゅ', ' zy u')
    text = text.replace('ずょ', ' zy o')
    text = text.replace('ずぇ', ' z e')
    text = text.replace('ずぉ', ' z o')
    text = text.replace('きゃ', ' ky a')
    text = text.replace('きゅ', ' ky u')
    text = text.replace('きょ', ' ky o')
    text = text.replace('しゃ', ' sh a')
    text = text.replace('しゅ', ' sh u')
    text = text.replace('しぇ', ' sh e')
    text = text.replace('しょ', ' sh o')
    text = text.replace('ちゃ', ' ch a')
    text = text.replace('ちゅ', ' ch u')
    text = text.replace('ちぇ', ' ch e')
    text = text.replace('ちょ', ' ch o')
    text = text.replace('とぅ', ' t u')
    text = text.replace('とゃ', ' ty a')
    text = text.replace('とゅ', ' ty u')
    text = text.replace('とょ', ' ty o')
    text = text.replace('どぁ', ' d o a')
    text = text.replace('どぅ', ' d u')
    text = text.replace('どゃ', ' dy a')
    text = text.replace('どゅ', ' dy u')
    text = text.replace('どょ', ' dy o')
    text = text.replace('どぉ', ' d o:')
    text = text.replace('にゃ', ' ny a')
    text = text.replace('にゅ', ' ny u')
    text = text.replace('にょ', ' ny o')
    text = text.replace('ひゃ', ' hy a')
    text = text.replace('ひゅ', ' hy u')
    text = text.replace('ひょ', ' hy o')
    text = text.replace('みゃ', ' my a')
    text = text.replace('みゅ', ' my u')
    text = text.replace('みょ', ' my o')
    text = text.replace('りゃ', ' ry a')
    text = text.replace('りゅ', ' ry u')
    text = text.replace('りょ', ' ry o')
    text = text.replace('ぎゃ', ' gy a')
    text = text.replace('ぎゅ', ' gy u')
    text = text.replace('ぎょ', ' gy o')
    text = text.replace('ぢぇ', ' j e')
    text = text.replace('ぢゃ', ' j a')
    text = text.replace('ぢゅ', ' j u')
    text = text.replace('ぢょ', ' j o')
    text = text.replace('じぇ', ' j e')
    text = text.replace('じゃ', ' j a')
    text = text.replace('じゅ', ' j u')
    text = text.replace('じょ', ' j o')
    text = text.replace('びゃ', ' by a')
    text = text.replace('びゅ', ' by u')
    text = text.replace('びょ', ' by o')
    text = text.replace('ぴゃ', ' py a')
    text = text.replace('ぴゅ', ' py u')
    text = text.replace('ぴょ', ' py o')
    text = text.replace('うぁ', ' u a')
    text = text.replace('うぃ', ' w i')
    text = text.replace('うぇ', ' w e')
    text = text.replace('うぉ', ' w o')
    text = text.replace('ふぁ', ' f a')
    text = text.replace('ふぃ', ' f i')
    text = text.replace('ふぅ', ' f u')
    text = text.replace('ふゃ', ' hy a')
    text = text.replace('ふゅ', ' hy u')
    text = text.replace('ふょ', ' hy o')
    text = text.replace('ふぇ', ' f e')
    text = text.replace('ふぉ', ' f o')
    # 1音からなる変換規則
    text = text.replace('あ', ' a')
    text = text.replace('い', ' i')
    text = text.replace('う', ' u')
    text = text.replace('え', ' e')
    text = text.replace('お', ' o')
    text = text.replace('か', ' k a')
    text = text.replace('き', ' k i')
    text = text.replace('く', ' k u')
    text = text.replace('け', ' k e')
    text = text.replace('こ', ' k o')
    text = text.replace('さ', ' s a')
    text = text.replace('し', ' sh i')
    text = text.replace('す', ' s u')
    text = text.replace('せ', ' s e')
    text = text.replace('そ', ' s o')
    text = text.replace('た', ' t a')
    text = text.replace('ち', ' ch i')
    text = text.replace('つ', ' ts u')
    text = text.replace('て', ' t e')
    text = text.replace('と', ' t o')
    text = text.replace('な', ' n a')
    text = text.replace('に', ' n i')
    text = text.replace('ぬ', ' n u')
    text = text.replace('ね', ' n e')
    text = text.replace('の', ' n o')
    text = text.replace('は', ' h a')
    text = text.replace('ひ', ' h i')
    text = text.replace('ふ', ' f u')
    text = text.replace('へ', ' h e')
    text = text.replace('ほ', ' h o')
    text = text.replace('ま', ' m a')
    text = text.replace('み', ' m i')
    text = text.replace('む', ' m u')
    text = text.replace('め', ' m e')
    text = text.replace('も', ' m o')
    text = text.replace('ら', ' r a')
    text = text.replace('り', ' r i')
    text = text.replace('る', ' r u')
    text = text.replace('れ', ' r e')
    text = text.replace('ろ', ' r o')
    text = text.replace('が', ' g a')
    text = text.replace('ぎ', ' g i')
    text = text.replace('ぐ', ' g u')
    text = text.replace('げ', ' g e')
    text = text.replace('ご', ' g o')
    text = text.replace('ざ', ' z a')
    text = text.replace('じ', ' j i')
    text = text.replace('ず', ' z u')
    text = text.replace('ぜ', ' z e')
    text = text.replace('ぞ', ' z o')
    text = text.replace('だ', ' d a')
    text = text.replace('ぢ', ' j i')
    text = text.replace('づ', ' z u')
    text = text.replace('で', ' d e')
    text = text.replace('ど', ' d o')
    text = text.replace('ば', ' b a')
    text = text.replace('び', ' b i')
    text = text.replace('ぶ', ' b u')
    text = text.replace('べ', ' b e')
    text = text.replace('ぼ', ' b o')
    text = text.replace('ぱ', ' p a')
    text = text.replace('ぴ', ' p i')
    text = text.replace('ぷ', ' p u')
    text = text.replace('ぺ', ' p e')
    text = text.replace('ぽ', ' p o')
    text = text.replace('や', ' y a')
    text = text.replace('ゆ', ' y u')
    text = text.replace('よ', ' y o')
    text = text.replace('わ', ' w a')
    text = text.replace('ゐ', ' i')
    text = text.replace('ゑ', ' e')
    text = text.replace('ん', ' N')
    text = text.replace('っ', ' q')
    text = text.replace('ー', ':')

    # ここまでに処理されてない ぁぃぅぇぉ はそのまま大文字扱い
    text = text.replace('ぁ', ' a')
    text = text.replace('ぃ', ' i')
    text = text.replace('ぅ', ' u')
    text = text.replace('ぇ', ' e')
    text = text.replace('ぉ', ' o')
    text = text.replace('ゎ', ' w a')
    text = text.replace('ぉ', ' o')
    # その他特別なルール
    text = text.replace('を', ' o')
    return text


def segment(wav, text, **conf):
    julius = Segmentation(**conf)
    return julius.segmente(wav, text)


def dictation(wav, **conf):
    julius = Dictation(**conf)
    res = julius.dictation(wav)
    julius.kill()
    return res


if __name__ == "__main__":
    from argparse import ArgumentParser
    from json import dumps
    parser = ArgumentParser(
        prog='julius.py',
        usage='python julius.py -w ../misc/sample.wav -t きょーわいいてんきだ',
        description='Speech Segmentation for WAV',
        add_help=True,
    )
    parser.add_argument(
      '-w', '--wav',
      help='wav file path',
      required=True
    )
    parser.add_argument(
        '-t', '--text',
        help='contents of the wav',
    )
    parser.add_argument(
        '-m', '--main_conf',
        default='/dictation-kit/main.jconf',
        help='path to main.jconf (for dictation mode)',
    )
    parser.add_argument(
        '-a', '--am_conf',
        default='/dictation-kit/am-dnn.jconf',
        help='path to am-dnn.jconf (for dictation mode)',
    )
    parser.add_argument(
        '-d', '--dnn_conf',
        default='/dictation-kit/julius.dnnconf',
        help='path to julius.dnnconf (for dictation mode)',
    )

    args = parser.parse_args()
    if args.text:
        result = segment(path.abspath(args.wav), args.text)
    else:
        conf = {
            'main_conf': args.main_conf,
            'am_conf': args.am_conf,
            'dnn_conf': args.dnn_conf
        }
        result = dictation(path.abspath(args.wav), **conf)

    print(dumps(
        {
            'result': result,
            'length': len(result)
        },
        ensure_ascii=False, indent=4, sort_keys=True
    ))
