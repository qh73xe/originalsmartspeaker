# -*- coding: utf-8 -*
"""音響特徴量解析APIサーバー

このファイルでは基本的に各種 API 自身の作成のみを行います.
種々の解析自体はそれぞれ別途モジュールを呼び出します.
"""
import json
import soundfile as sf
from os import path
from io import BytesIO

from tornado.web import RequestHandler, Application
from tornado.escape import json_decode
from tornado.options import define, options

from pyworld import dio, stonemask, cheaptrick, d4c, wav2world, synthesize
define("port", default=8888, help="run on the given port", type=int)

ROOT = path.dirname(path.abspath(__file__))
TEMPDIR = path.join(ROOT, 'tmp')


class MainHandler(RequestHandler):

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def get(self):
        result = {
            'title': 'Wellcom to Acoustic-Analysis-API',
            'description': 'you can get the text of wav.',
            'details': [
                {
                    'url': self.reverse_url('index'),
                    'description': 'This page.'
                },
                {
                    'url': self.reverse_url('world'),
                    'description': 'Get F0 from wavefile using world.'
                }
            ]
        }
        self.write(json.dumps(
            result, ensure_ascii=False
        ))


class BaseHandler(RequestHandler):
    """音響処理系の基底ハンドラ

    JSON または fileform から wav データを取り出し,
    一時的に保存します.

    また, 処理の最後に, 取得した wavfile は基本的に削除します.
    """

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def get(self):
        result = {
            'status': 'error',
            'reason': 'get method is not supported.'
        }
        self.write(json.dumps(
            result, ensure_ascii=False
        ))

    def upload_base64(self, enced_base64):
        from base64 import b64decode
        body = b64decode(enced_base64)
        fileobj = BytesIO(body)
        return sf.read(fileobj)

    def upload_file(self, body):
        fileobj = BytesIO(body)
        return sf.read(fileobj)

    def write_wav(self, x, fs):
        fileobj = BytesIO()
        fileobj.name = 'result.wav'
        sf.write(fileobj, x, fs)
        return fileobj


class WorldHandler(BaseHandler):
    """World を使い 音響特徴量の抽出を行います."""

    def synthesize(self, f0, sp, ap, fs):
        y = synthesize(f0, sp, ap, fs)
        return y, fs

    def get_features(self, x, fs, stone_mask=True):
        f0, sp, ap = wav2world(x, fs)
        return x, fs, f0, sp, ap

    def get_f0(self, x, fs, stone_mask=True):
        f0, t = dio(x, fs)
        if stone_mask:
            f0 = stonemask(x, f0, t, fs)
        return x, fs, f0, t

    def get_sp(self, x, fs,  stone_mask=True):
        x, fs, f0, t = self.get_f0(x, fs, stone_mask)
        sp = cheaptrick(x, f0, t, fs)
        return x, fs, sp, t

    def get_ap(self, x, fs, stone_mask=True):
        x, fs, f0, t = self.get_f0(x, fs, stone_mask)
        ap = d4c(x, f0, t, fs)
        return x, fs, ap, t

    def post(self, feature):
        feature = feature.lower()
        try:
            request_json = json_decode(self.request.body)
        except Exception:
            # ファイルが直接 UPLOAD された場合
            results = []
            for key, fileinfo in self.request.files.items():
                x, fs = self.upload_file(fileinfo[0]['body'])
                if feature == 'f0':
                    x, fs, f0, t = self.get_f0(x, fs)
                    results.append({
                        'f0': f0.tolist(),
                        'time': t.tolist(),
                        'sample_rate': fs
                    })
                elif feature == 'ap':
                    x, fs, ap, t = self.get_ap(x, fs)
                    results.append({
                        'aperiodic': ap.tolist(),
                        'time': t.tolist(),
                        'sample_rate': fs
                    })
                elif feature == 'sp':
                    x, fs, sp, t = self.get_sp(x, fs)
                    results.append({
                        'spectral_envelope': sp.tolist(),
                        'time': t.tolist(),
                        'sample_rate': fs
                    })
                else:
                    x, fs, f0, sp, ap = self.get_features(x, fs)
                    results.append({
                        'f0': f0.tolist(),
                        'aperiodic': ap.tolist(),
                        'spectral_envelope': sp.tolist(),
                        'sample_rate': fs
                    })
            self.write(json.dumps({
                'status': 'success',
                'result': results,
                'length': len(results)
            }, ensure_ascii=False))
        else:
            if feature == 'synthesize':
                from numpy import array
                if isinstance(request_json, list):
                    request_json = request_json[0]
                f0 = array(request_json.get('f0'))
                sp = array(request_json.get('spectral_envelope'))
                ap = array(request_json.get('aperiodic'))
                fs = request_json.get('sample_rate', None)
                if f0.any() and sp.any() and ap.any() and fs is not None:
                    y, fs = self.synthesize(f0, sp, ap, fs)
                    self.set_header('Content-type',  'audio/wav')
                    fileobj = self.write_wav(y, fs)
                    self.write(fileobj.getvalue())
                else:
                    self.write(json.dumps({
                        'status': 'error',
                        'result': [],
                        'length': 0
                    }, ensure_ascii=False))
            else:
                body = request_json.get('file')
                x, fs = self.upload_base64(body)
                if feature == 'f0':
                    x, fs, f0, t = self.get_f0(x, fs)
                    results.append({
                        'f0': f0.tolist(),
                        'time': t.tolist(),
                        'sample_rate': fs
                    })
                elif feature == 'ap':
                    x, fs, ap, t = self.get_ap(x, fs)
                    results.append({
                        'aperiodic': ap.tolist(),
                        'time': t.tolist(),
                        'sample_rate': fs
                    })
                elif feature == 'sp':
                    x, fs, sp, t = self.get_sp(x, fs)
                    results.append({
                        'spectral_envelope': sp.tolist(),
                        'time': t.tolist(),
                        'sample_rate': fs
                    })
                else:
                    x, fs, f0, sp, ap = self.get_features(x, fs)
                    results.append({
                        'f0': f0.tolist(),
                        'aperiodic': ap.tolist(),
                        'spectral_envelope': sp.tolist(),
                        'sample_rate': fs
                    })
                self.write(json.dumps({
                    'status': 'success',
                    'result': results,
                    'length': len(results)
                }, ensure_ascii=False))
        self.finish()


def make_app():
    """URL 設計関数."""
    from tornado.web import url
    return Application([
        url(r"/", MainHandler, name="index"),
        url(r"/world/(?P<feature>\w+)", WorldHandler, name="world"),
    ])


if __name__ == "__main__":
    from tornado.ioloop import IOLoop
    options.parse_command_line()
    app = make_app()
    app.listen(options.port)
    ioloop = IOLoop.current()
    try:
        print(
            'Acoustic-Analysis-API: START WITH http://localhost:{}'.format(
              str(options.port)
            )
        )
        ioloop.start()
    except KeyboardInterrupt:
        print('Speech-Recognition-API: END')
        ioloop.stop()
