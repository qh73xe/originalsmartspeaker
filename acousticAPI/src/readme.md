# Acoustic-Analysis-API

音響解析ツール world を利用して wav ファイルから, f0, スペクトル崩落, 非周期性指標を算出します.
また, これらの情報を json 形式で渡すことにより音声合成を行うことが可能です.

## How to use this image

以下のコマンドを実行することで API サーバーを立てることができます.

```
$ docker run -it -p 8888:8888 qh73xe/world
```

このコマンドを実行するとローカル環境の 8888 ポートに音響解析サーバーが立ち上がります.
このサーバーは以下のアクセスポイントを持ちます.

- http:localhost:8888/ : index ページ
- http:localhost:8888/world/f0 : f0 解析用アクセスポイント
- http:localhost:8888/world/sp : スペクトル崩落解析用アクセスポイント
- http:localhost:8888/world/ap : 非周期性指標解析用アクセスポイント
- http:localhost:8888/world/all : 上記 3 をまとめて解析
- http:localhost:8888/world/synthesize : 種々の音響特徴量から音声合成を行う

## Example

以下にコマンドラインを使った解析例を示します.
なお, この例の実行には curl 及び jq (json パース用) が必要になります.

- jq: `$ brew install jq`

### F0 の値を取得する.

```
$ curl -F file=@misc/sample.wav http://localhost:8888/world/f0
```

- -F 以下は適当な wav ファイルを渡してください.
- その他, スペクトル崩落や非周期性指標は, url 末尾を変更するのみで試せるかと思います.


### 音声合成をする

```
$ curl -F file=@misc/sample.wav http://localhost:8888/world/all > test.json
$ cat test.json | jq '.result' | curl -X POST -H "Content-Type: application/json" -d @- http://localhost:8888/world/synthesize --output test.wav
```

この例では一行目で misc/sample.wav の音響解析を行い結果を test.json に保存しています.
続く二行目では, 解析した結果をそのまま再合成にかけています.
まずは `cat test.json` で結果を表示させます.
そのうち, 再合成に使用するのは 'result' の中身だけなので, jq コマンドでその部分のみを取り出しています.
最後にその結果を curl に渡します.

- curl のオプションの内, `-d @-` の部分はパイプの値をここに挿入するという意味です.
- このアクセスポイントでは, 再合成された wav ファイルを返すので --output オプションをつけています.


